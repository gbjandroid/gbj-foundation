package com.gbj.foundation.quickaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gbj.foundation.quickaction.QuickActionDialog

interface QuickActionViewInterface {
    fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, dialog: QuickActionDialog): View?
}

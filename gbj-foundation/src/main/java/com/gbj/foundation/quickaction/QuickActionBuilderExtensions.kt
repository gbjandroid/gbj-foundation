package com.gbj.foundation.quickaction

/**
 * sets the minimum height percent of the quick action
 * @param minHeightPercent a value from 0 to 1 that defines the minimum height percent
 */
fun QuickAction.Builder.setMinHeightPercent(minHeightPercent: Double) = apply {
    this.minHeightPercent = minHeightPercent
}

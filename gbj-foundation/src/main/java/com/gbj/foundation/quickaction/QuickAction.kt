package com.gbj.foundation.quickaction

import androidx.fragment.app.FragmentManager
import java.io.Serializable
import java.util.*
import kotlin.collections.HashMap

class QuickAction private constructor(builder: Builder) : Serializable {

    companion object {
        val quickActionViewMap = HashMap<String, QuickActionViewInterface?>()
    }

    val quickActionViewKey: String?
    val maxHeightPercent: Double
    val minHeightPercent: Double
    val removeHorizontalMargins: Boolean
    val removePaddingBottom: Boolean
    var canceledOnTouchOutside: Boolean = false
    var isDraggable: Boolean = false
    var cancelable: Boolean = true

    init {
        this.maxHeightPercent = builder.maxHeightPercent
        this.minHeightPercent = builder.minHeightPercent
        this.removeHorizontalMargins = builder.removeHorizontalMargins
        this.removePaddingBottom = builder.removePaddingBottom
        this.canceledOnTouchOutside = builder.canceledOnTouchOutside
        this.isDraggable = builder.isDraggable
        this.cancelable= builder.cancelable

        val uniqueKey = UUID.randomUUID().toString()
        this.quickActionViewKey = uniqueKey
        if (!quickActionViewMap.containsKey(uniqueKey)) {
            quickActionViewMap[uniqueKey] = builder.quickActionView
        }
    }

    class Builder(private val fragmentManager: FragmentManager) {
        var tag: String? = null
            private set
        var quickActionView: QuickActionViewInterface? = null
            private set
        var maxHeightPercent: Double = 0.95
            private set
        internal var minHeightPercent: Double = -1.0
        var quickActionBackgroundType: QuickActionBackgroundType = QuickActionBackgroundType.WHITE
            private set
        var removeHorizontalMargins: Boolean = false
            private set
        var removePaddingBottom: Boolean = false
            private set

        var cancelable: Boolean = true
            private set

        var canceledOnTouchOutside: Boolean = false
            private set

        var isDraggable: Boolean = false
            private set

        /**
         * sets the maximum height percent of the quick action, the default value is 0.95
         * @param maxHeightPercent a value from 0 to 1 that defines the maximum height percent
         */
        fun setMaxHeightPercent(maxHeightPercent: Double) = apply { this.maxHeightPercent = maxHeightPercent }

        fun setTag(tag: String) = apply { this.tag = tag }

        fun setRemoveHorizontalMargins(removeHorizontalMargins: Boolean) =
                apply { this.removeHorizontalMargins = removeHorizontalMargins }

        fun setCancelable(cancelable: Boolean) =
            apply { this.cancelable = cancelable }

        fun setRemovePaddingBottom(removePaddingBottom: Boolean) =
                apply { this.removePaddingBottom = removePaddingBottom }

        fun setQuickActionDialogView(quickActionView: QuickActionViewInterface) =
                apply { this.quickActionView = quickActionView }

        fun setQuickActionBackgroundType(quickActionBackgroundType: QuickActionBackgroundType) =
                apply { this.quickActionBackgroundType = quickActionBackgroundType }

        fun setCanceledOnTouchOutside(canceledOnTouchOutside: Boolean) =
                apply { this.canceledOnTouchOutside = canceledOnTouchOutside }

        fun show(): QuickActionDialog {
            val quickActionDialog = QuickActionDialog.newInstance(QuickAction(this))
            quickActionDialog.show(fragmentManager, tag)
            return quickActionDialog
        }
    }
}

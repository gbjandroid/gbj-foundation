package com.gbj.foundation.quickaction

import com.gbj.foundation.quickaction.QuickAction
import com.gbj.foundation.quickaction.QuickActionDismissState

class QuickActionViewModel(private val quickAction: QuickAction) {

    var quickActionDismissState: QuickActionDismissState? = null

    val isPaddingBottomVisible: Boolean = !quickAction.removePaddingBottom

    val shouldRemoveHorizontalMargins: Boolean
        get() = quickAction.removeHorizontalMargins

}

package com.gbj.foundation.quickaction

import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import com.gbj.foundation.R
import com.gbj.foundation.databinding.LayoutQuickActionBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class QuickActionDialog : BottomSheetDialogFragment() {

    internal lateinit var quickActionViewModel: QuickActionViewModel
    private lateinit var layoutQuickActionBinding: LayoutQuickActionBinding

    companion object {
        private const val QUICK_ACTION_KEY = "quick_action_key"

        fun newInstance(quickAction: QuickAction): QuickActionDialog {
            val quickActionDialog = QuickActionDialog()
            val bundle = Bundle()
            bundle.putSerializable(QUICK_ACTION_KEY, quickAction)
            quickActionDialog.arguments = bundle
            return quickActionDialog
        }
    }

    private val quickAction: QuickAction by lazy { arguments?.getSerializable(QUICK_ACTION_KEY) as QuickAction }
    private var quickActionView: QuickActionViewInterface? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.QuickActionDialogStyle)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        layoutQuickActionBinding = LayoutQuickActionBinding.inflate(inflater, container, false)
        getQuickActionView(quickAction)
        quickActionViewModel = QuickActionViewModel(quickAction)
        configureQuickActionViews(layoutQuickActionBinding)
        addExternalView(layoutQuickActionBinding, quickActionView, inflater, container)
        configureQuickActionDialog(quickAction, layoutQuickActionBinding)
        return layoutQuickActionBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setWindowAnimations(R.style.QuickActionDialogAnimation)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        quickAction.quickActionViewKey?.let {
            QuickAction.quickActionViewMap[it] = quickActionView
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        getQuickActionView(quickAction)
    }

    private fun getQuickActionView(quickAction: QuickAction) {
        quickAction.quickActionViewKey?.let {
            if (QuickAction.quickActionViewMap.containsKey(it)) {
                quickActionView = QuickAction.quickActionViewMap[it]
                QuickAction.quickActionViewMap.remove(it)
            }
        }
    }

    private fun configureQuickActionViews(rootView: LayoutQuickActionBinding) {
        rootView.quickActionMainView.setBackgroundResource(R.drawable.account_background_with_radius)
    }

    private fun addExternalView(
        rootView: LayoutQuickActionBinding,
        quickActionView: QuickActionViewInterface?,
        inflater: LayoutInflater,
        container: ViewGroup?,
    ) {
        quickActionView?.onCreateView(inflater, container, this)?.let {
            if (quickActionViewModel.shouldRemoveHorizontalMargins) {
                val params =
                    rootView.quickActionContainer.layoutParams as LinearLayout.LayoutParams
                params.marginStart = 0
                params.marginEnd = 0
            }
            if (!quickActionViewModel.isPaddingBottomVisible) {
                rootView.quickActionContainer.setPadding(
                    rootView.quickActionContainer.paddingEnd,
                    rootView.quickActionContainer.paddingTop,
                    rootView.quickActionContainer.paddingRight,
                    0
                )
            }
            rootView.quickActionContainer.addView(it)
        }
    }

    private fun configureQuickActionDialog(
        quickAction: QuickAction,
        rootView: LayoutQuickActionBinding
    ) {
        dialog?.setOnShowListener {

            // Change bottom sheet background to transparent to show curve corners effect
            (it as BottomSheetDialog).findViewById<FrameLayout>(R.id.design_bottom_sheet)
                ?.also { bottomSheet ->
                    bottomSheet.background = null

                    // Set the maximum height for the bottom sheet
                    val metrics = DisplayMetrics()
                    (bottomSheet.context?.getSystemService(Context.WINDOW_SERVICE) as WindowManager)
                        .defaultDisplay.getRealMetrics(metrics)
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
                        bottomSheet.context?.display?.getRealMetrics(metrics)
                    }
                    val maxDialogHeight = metrics.heightPixels.coerceAtMost(
                        (metrics.heightPixels * quickAction.maxHeightPercent).toInt()
                    )
                    BottomSheetBehavior.from(bottomSheet).peekHeight = maxDialogHeight

                    // apply minimum height
                    // when min value > peek, the default behaviour will apply peekHeight and ignore minimumHeight
                    if (quickAction.minHeightPercent >= 0) {
                        val minHeight =
                            resources.displayMetrics.heightPixels * quickAction.minHeightPercent
                        rootView.quickActionMainView.minimumHeight = minHeight.toInt()
                    }

                    it.setCanceledOnTouchOutside(quickAction.canceledOnTouchOutside)
                    BottomSheetBehavior.from(bottomSheet).apply {
                        isDraggable = quickAction.isDraggable
                        isCancelable = quickAction.cancelable
                        addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                            override fun onStateChanged(bottomSheet: View, newState: Int) {
                                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                                    quickActionViewModel.quickActionDismissState =
                                        QuickActionDismissState.ON_DIALOG_DRAGGED_TO_DISMISS
                                }
                            }

                            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                                // no need
                            }
                        })
                    }

                    it.setOnKeyListener { _, keyCode, event ->
                        if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
                            quickActionViewModel.quickActionDismissState =
                                QuickActionDismissState.ON_BACK_PRESSED
                        }
                        false
                    }
                }
        }
    }

}

package com.gbj.foundation.quickaction

enum class QuickActionDismissState {
    ON_DIALOG_DRAGGED_TO_DISMISS,
    ON_BACK_PRESSED,
}

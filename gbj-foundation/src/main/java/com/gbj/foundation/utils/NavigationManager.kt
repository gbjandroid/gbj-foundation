package com.gbj.foundation.utils

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import androidx.navigation.Navigator
import androidx.navigation.NavigatorProvider
import androidx.navigation.fragment.FragmentNavigator
import com.gbj.foundation.gbjlayout.GBJLayoutConfig
import com.gbj.foundation.gbjlayout.GBJ_LAYOUT_CONFIG_KEY
import org.jetbrains.annotations.NotNull
import kotlin.jvm.Throws

object NavigationManager {

    val navigateToDashboard = MutableLiveData<SingleLiveDataEvent<Boolean>>()

    fun navigateTo(
        @NotNull activity: Activity,
        @NotNull @IdRes navHostId: Int,
        @NotNull navigationProperties: NavigationProperties,
        @NotNull bundle: Bundle = Bundle()
    ) {
        navigationProperties.gbjLayoutConfig?.let { it ->
            bundle.putSerializable(
                GBJ_LAYOUT_CONFIG_KEY,
                it
            )
        }
        Navigation.findNavController(activity, navHostId)
            .navigate(navigationProperties.actionId, bundle)
    }

    fun navigateTo(
        @NotNull view: View,
        @NotNull navigationProperties: NavigationProperties,
        @NotNull bundle: Bundle = Bundle()
    ) {
        navigationProperties.gbjLayoutConfig?.let { it ->
            bundle.putSerializable(
                GBJ_LAYOUT_CONFIG_KEY,
                it
            )
        }
        Navigation.findNavController(view).navigate(navigationProperties.actionId, bundle)
    }

    fun navigateWithExtrasTo(
        @NotNull view: View,
        @NotNull navigationProperties: NavigationProperties,
        @NotNull navigatorExtras: Navigator.Extras?,
        @NotNull bundle: Bundle = Bundle()
    ) {
        navigationProperties.gbjLayoutConfig?.let { it ->
            bundle.putSerializable(
                GBJ_LAYOUT_CONFIG_KEY,
                it
            )
        }
        Navigation.findNavController(view).navigate(
            navigationProperties.actionId,
            bundle,
            null,
            navigatorExtras
        )
    }

    fun <T : Fragment> navigateTo(
        @NotNull view: View,
        @NotNull fragmentClass: Class<T>,
        @NotNull navigationProperties: NavigationProperties,
        @NotNull bundle: Bundle = Bundle()
    ) {
        try {
            navigateTo(view, navigationProperties, bundle)
        } catch (_: Exception) {
            addDynamicDestinationToNavGraph(view, fragmentClass, navigationProperties.actionId)
            navigateTo(view, navigationProperties, bundle)
        }
    }

    @Throws(NavigationException::class)
    fun <T : Fragment> addDynamicDestinationToNavGraph(
        @NotNull view: View,
        @NotNull fragmentClass: Class<T>,
        @NotNull destinationId: Int
    ) {
        val navController = try {
            Navigation.findNavController(view)
        } catch (e: java.lang.IllegalStateException) {
            throw NavigationException(
                "Could not find navigation " +
                        "controller from the provided view. Please make sure you are " +
                        "using NavHost."
            )
        }
        val destination = navController.navigatorProvider
            .getNavigatorOrThrow(FragmentNavigator::class.java)
            .createDestination()
            .apply {
                id = destinationId
                label = fragmentClass.simpleName
                className = fragmentClass.name
            }
        navController.graph.addDestination(destination)
    }

    fun navigateUp(@NotNull view: View) {
        Navigation.findNavController(view).navigateUp()
    }

    class NavigationException(message: String, e: Exception? = null) :
        IllegalStateException(message, e)

    @Throws(NavigationException::class)
    private fun <T : Navigator<*>> NavigatorProvider.getNavigatorOrThrow(navigatorClass: Class<T>): T {
        return try {
            getNavigator(navigatorClass)
        } catch (e: IllegalStateException) {
            throw NavigationException(
                "Could not obtain navigator" +
                        " '${navigatorClass.simpleName}' from the view's Navigation Controller."
            )
        }
    }
}

const val GBJ_NEW_GRAPH_KEY = "gbj_new_graph_key"
const val GBJ_NEW_GRAPH_FIRST_ACTION_KEY = "gbj_new_graph_first_action_key"

data class NavigationProperties(val actionId: Int, val gbjLayoutConfig: GBJLayoutConfig? = null)

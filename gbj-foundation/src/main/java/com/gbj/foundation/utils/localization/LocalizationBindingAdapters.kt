package com.gbj.foundation.utils.localization

import android.widget.EditText
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.google.android.material.tabs.TabLayout

object LocalizationBindingAdapters {

    @JvmStatic
    @BindingAdapter("app:tabsTextByKey")
    fun setTabTextByKey(view: TabLayout, keyList: List<TabText>?) {
        keyList?.forEach {
            view.getTabAt(it.position)?.text = GBJContentManager.getValue(it.textKey)
        }
    }

    @JvmStatic
    @BindingAdapter("app:textByKey", "app:textByKeyFormatArgs", requireAll = false)
    fun setTextViewTextFromString(textView: TextView, key: String?, format: Array<String>? = null) {
        textView.text = GBJContentManager.getValue(key, format ?: arrayOf())
    }

    @JvmStatic
    @BindingAdapter("app:textByKeyWithContentUpdateListener", "app:textByKeyFormatArgs", requireAll = false)
    fun setTextViewTextFromStringWithContentUpdateListener(
            textView: TextView,
            key: String?,
            format: Array<String>? = null
    ) {
        setTextViewTextFromString(textView, key, format)
        key?.also {
            GBJContentManager.addContentMapUpdateListener(it) {
                setTextViewTextFromString(textView, key, format)
            }
        }
    }

    @JvmStatic
    @BindingAdapter("app:hintByKey")
    fun setEditTextHintFromString(editableText: EditText, key: String?) {
        editableText.hint = GBJContentManager.getValue(key)
    }
}

data class TabText(val textKey: String, val position: Int)

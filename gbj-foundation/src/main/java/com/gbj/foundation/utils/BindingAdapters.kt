package com.gbj.foundation.utils

import android.content.ContentResolver
import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Html
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory
import com.gbj.foundation.utils.extensions.getDrawableByName
import com.gbj.foundation.utils.extensions.getDrawableResIdByName
import java.util.regex.Pattern

object BindingAdapters {

    @JvmStatic
    @BindingAdapter("app:src")
    fun setImageResource(imageView: ImageView, resourceName: String?) {
        resourceName?.let {
            val context = imageView.context
            val resource =
                context.resources.getIdentifier(resourceName, "drawable", context.packageName)
            imageView.setImageResource(resource)
        }
    }

    @JvmStatic
    @BindingAdapter("app:src")
    fun setImageResource(imageView: ImageView, resourceId: Int?) {
        resourceId?.let { imageView.setImageResource(resourceId) }
    }

    @JvmStatic
    @BindingAdapter("app:srcBackground")
    fun setBackgroundImageResource(view: ViewGroup, resourceId: Int?) {
        resourceId?.let {
            view.setBackgroundResource(it)
        }
    }

    @JvmStatic
    @BindingAdapter(
        value = ["app:imageUrl", "app:placeHolderResId",
            "app:placeHolderResKey", "app:tintColor"], requireAll = false
    )
    fun loadImageUrl(
        imageView: ImageView,
        url: String? = null,
        placeHolderResId: Int? = null,
        placeHolderResKey: String? = null,
        tintColor: Int? = null
    ) {
        val placeHolderDrawable: Drawable? = when {
            placeHolderResId != null -> ContextCompat.getDrawable(
                imageView.context,
                placeHolderResId
            )
            placeHolderResKey != null -> imageView.context.getDrawableByName(placeHolderResKey)
            else -> null
        }
        placeHolderDrawable?.run {
            tintColor?.let {
                DrawableCompat.setTint(this, it)
            }
        }
        loadImage(url, imageView, placeHolderDrawable)
    }

    private fun loadImage(url: String?, imageView: ImageView, placeHolder: Drawable?) {
        if (url?.isNotBlank() == true) {
            val drawableRes = getDrawableResourceIdFromUrl(imageView.context, url)
            if (drawableRes != null) {
                imageView.setImageResource(drawableRes)
            } else {
                Glide.with(imageView)
                    .load(url)
                    .transition(
                        DrawableTransitionOptions.withCrossFade(
                            DrawableCrossFadeFactory.Builder()
                                .setCrossFadeEnabled(true)
                                .build()
                        )
                    )
                    .placeholder(placeHolder)
                    .into(imageView)
            }
        } else {
            imageView.setImageDrawable(placeHolder)
        }
    }

    private fun getDrawableResourceIdFromUrl(context: Context, url: String) =
        getDrawableNameFromUrl(context, url)?.let { context.getDrawableResIdByName(it) }

    private fun getDrawableNameFromUrl(context: Context, url: String): String? {
        val pattern =
            Pattern.compile("${ContentResolver.SCHEME_ANDROID_RESOURCE}://${context.packageName}/drawable/(.+)")
        val matcher = pattern.matcher(url)
        if (matcher.matches()) {
            return matcher.group(1).takeUnless { it.isNullOrBlank() }
        }
        return null
    }

    @JvmStatic
    @BindingAdapter("htmltext")
    fun setHtmlText(textView: TextView, html: String?) {
        html?.let {
            textView.text = Html.fromHtml(it)
        }
    }

}

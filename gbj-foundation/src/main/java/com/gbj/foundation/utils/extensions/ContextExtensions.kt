package com.gbj.foundation.utils.extensions

import android.content.Context
import android.content.res.Resources
import androidx.core.content.ContextCompat

fun Context.getResourceName(resId: Int?): String? {
    return resId?.let {
        try {
            resources.getResourceEntryName(resId)
        } catch (_: Resources.NotFoundException) {
            null
        }
    }
}

fun Context.getDrawableByName(name: String) =
    getDrawableResIdByName(name)?.let { ContextCompat.getDrawable(this, it) }

fun Context.getDrawableResIdByName(name: String): Int? {
    val drawableResId = resources.getIdentifier(name, "drawable", packageName)
    return drawableResId.takeUnless { it == 0 }
}

fun Context.getRawIdByName(name: String): Int? {
    val resId = resources.getIdentifier(name, "raw", packageName)
    return resId.takeUnless { it == 0 }
}

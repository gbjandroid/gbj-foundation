package com.gbj.foundation.gbjlayout

interface GBJLayoutConfiguration {
    fun updateGBJLayoutConfig(
            gbjLayoutConfig: GBJLayoutConfig?
    )
}

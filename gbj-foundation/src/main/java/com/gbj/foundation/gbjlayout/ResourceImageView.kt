package com.gbj.foundation.gbjlayout

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView

/**
 * An ImageView that saves the last set Image resource so that it can be checked by users to know which resource is currently being displayed.
 */
class ResourceImageView(context: Context, attributeSet: AttributeSet?) : AppCompatImageView(context, attributeSet) {

    @DrawableRes
    var currentDrawableRes: Int? = null
        private set

    constructor(context: Context) : this(context, null)

    override fun setImageResource(@DrawableRes resId: Int) {
        currentDrawableRes = resId
        super.setImageResource(resId)
    }
}

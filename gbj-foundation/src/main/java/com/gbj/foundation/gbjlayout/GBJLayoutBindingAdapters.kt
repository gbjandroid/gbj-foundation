package com.gbj.foundation.gbjlayout

import android.widget.ImageSwitcher
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter

object GBJLayoutBindingAdapters {


    @JvmStatic
    @BindingAdapter("app:switchImageWithFade", requireAll = true)
    fun switchImageWithFade(
        imageSwitcher: ImageSwitcher,
        @DrawableRes imageRes: Int,
    ) {
        if ((imageSwitcher.currentView as? ResourceImageView)?.currentDrawableRes != imageRes) {
            imageSwitcher.setImageResource(imageRes)
        }
    }
}

package com.gbj.foundation.gbjlayout

import android.view.View
import androidx.annotation.ColorRes
import java.io.Serializable

const val GBJ_LAYOUT_CONFIG_KEY = "gbj_layout_config_key"

data class GBJLayoutConfig(
    val toolbar: ToolbarType,
    val background: GBJBackgroundType,
    val title: String = "",
    val toolbarColor: GBJToolbarColor = GBJToolbarColor.WHITE_WITH_SEPARATOR,
    @ColorRes val toolbarElementsColor: Int = View.NO_ID,
    val customBGConfig: GBJLayoutBGCustomConfig? = null
) : Serializable

interface ToolbarType {
    val name: String
}

enum class AllToolbarTypes : ToolbarType {
    NONE, ONBOARDING, ONBOARDING_DIMMED_TOOLBAR, MODULE_STARTUP_SCREEN, MODULE_INNER_SCREEN
}

enum class GBJToolbarType : ToolbarType {
    NONE, MODULE_STARTUP_SCREEN, MODULE_INNER_SCREEN
}

enum class GBJBackgroundType {
    WHITE,
    BALTIC_SEA,
    GREY_GRADIENT,
    ERROR_RED_GRADIENT,
    CUSTOM
}

enum class GBJToolbarColor {
    WHITE, TRANSPARENT, WHITE_WITH_SEPARATOR
}

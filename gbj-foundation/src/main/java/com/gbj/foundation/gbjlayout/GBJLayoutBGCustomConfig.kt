package com.gbj.foundation.gbjlayout

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import java.io.Serializable

data class GBJLayoutBGCustomConfig(
        @DrawableRes
        val backgroundResId: Int,
        @ColorRes
        val tintColorResId: Int
) : Serializable

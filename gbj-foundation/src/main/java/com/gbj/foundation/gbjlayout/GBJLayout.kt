package com.gbj.foundation.gbjlayout

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.ColorRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.gbj.foundation.R
import com.gbj.foundation.databinding.LayoutGbjBinding

class GBJLayout(context: Context, attr: AttributeSet) : FrameLayout(context, attr) {

    private val viewModel = GBJLayoutViewModel()

    private var binding: LayoutGbjBinding =
        LayoutGbjBinding.inflate(LayoutInflater.from(context), this, true).also {
            it.viewModel = viewModel
        }

    fun updateLayout(model: GBJLayoutConfig?) {
        model?.let { viewModel.changeStyle(model) }
    }

    fun setOnCloseButtonClickListener(listener: OnClickListener?) {
        binding.closeIcon.setOnClickListener(listener)
    }

    fun setOnCloseButtonClickListener(listener: ((View) -> (Unit))?) {
        binding.closeIcon.setOnClickListener(listener)
    }

    fun setOnBackButtonClickListener(listener: OnClickListener?) {
        binding.btnBack.setOnClickListener(listener)
    }


    override fun addView(child: View, index: Int, params: ViewGroup.LayoutParams) {
        if (child.id == R.id.gbj_container) {
            super.addView(child, index, params)
        } else {
            binding.gbjLayoutContainer.addView(child, index, params)
        }
    }

    companion object {
        @JvmStatic
        @BindingAdapter("app:background")
        fun setViewBackground(view: View, resId: Int?) {
            resId?.let {
                view.setBackgroundResource(resId)
            }
        }

        @JvmStatic
        @BindingAdapter("app:tint")
        fun setImageViewTint(imageView: AppCompatImageView, @ColorRes resourceId: Int) {
            imageView.setColorFilter(ContextCompat.getColor(imageView.context, resourceId))
        }
    }
}

package com.gbj.foundation.gbjlayout

import android.view.View
import androidx.databinding.BaseObservable
import com.gbj.foundation.R
import com.gbj.foundation.gbjlayout.*
import com.gbj.foundation.BR

private const val CUSTOM_TINT_COLOR_RES_ERROR_MSG = "tint color resource cannot be null" +
        " with MVA10BackgroundType.Custom"
private const val CUSTOM_BACKGROUND_RES_ERROR_MSG = "background resource cannot be null" +
        " with MVA10BackgroundType.Custom"

class GBJLayoutViewModel : BaseObservable() {

    private var model: GBJLayoutConfig? = null

    fun changeStyle(mva10LayoutConfig: GBJLayoutConfig) {
        model = mva10LayoutConfig
        notifyPropertyChanged(BR._all)
    }

    fun getOnboardingToolbarVisibility(): Int {
        return when (model?.toolbar) {
            AllToolbarTypes.ONBOARDING -> View.VISIBLE
            AllToolbarTypes.ONBOARDING_DIMMED_TOOLBAR -> View.VISIBLE
            else -> View.GONE
        }
    }

    fun getOnboardingDimmedToolbarVisibility(): Int {
        return when (model?.toolbar) {
            AllToolbarTypes.ONBOARDING_DIMMED_TOOLBAR -> View.VISIBLE
            else -> View.GONE
        }
    }

    fun getDefaultToolbarVisibility(): Int {
        return when (model?.toolbar) {
            GBJToolbarType.MODULE_STARTUP_SCREEN -> View.VISIBLE
            GBJToolbarType.MODULE_INNER_SCREEN -> View.VISIBLE
            else -> View.GONE
        }
    }

    fun getDefaultToolbarBackVisibility(): Int {
        return when (model?.toolbar) {
            GBJToolbarType.MODULE_INNER_SCREEN -> View.VISIBLE
            else -> View.INVISIBLE
        }
    }

    fun getDefaultToolbarBackgroundColor(): Int {
        return when (model?.toolbarColor) {
            GBJToolbarColor.TRANSPARENT -> R.color.transparent
            else -> R.color.toolbarBgColor
        }
    }

    fun getDefaultToolbarSeparatorColor(): Int {
        return when (model?.toolbarColor) {
            GBJToolbarColor.WHITE_WITH_SEPARATOR -> R.color.separatorColor
            else -> R.color.transparent
        }
    }

    fun getTintColor(): Int {
        var tintColor = R.color.waveViewsTintColor
        model?.also {
            tintColor = if (it.toolbarElementsColor != View.NO_ID) {
                it.toolbarElementsColor
            } else {
                when (model?.background) {
                    GBJBackgroundType.WHITE -> R.color.colorBalticSea
                    GBJBackgroundType.CUSTOM -> {
                        val colorRes = checkNotNull(
                            model?.customBGConfig?.tintColorResId
                        ) { CUSTOM_TINT_COLOR_RES_ERROR_MSG }
                        colorRes
                    }
                    else -> R.color.waveViewsTintColor
                }
            }
        }
        return tintColor
    }

    fun getNavigationHeaderTitle() = model?.title ?: ""

    fun getBackground(): Int {
        return when (model?.background) {
            GBJBackgroundType.WHITE -> R.color.white
            GBJBackgroundType.BALTIC_SEA -> R.color.colorPrimary
            GBJBackgroundType.CUSTOM -> {
                val drawableRes = checkNotNull(
                    model?.customBGConfig?.backgroundResId
                ) { CUSTOM_BACKGROUND_RES_ERROR_MSG }
                drawableRes
            }
            else -> R.color.white
        }
    }
}
